#
# 7-1 Class for deck of cards
class Suit:
    Club = 1
    Diamond = 2
    Heart = 3
    Spade = 4

class Card:
    def __init__(self, number, suit):
        self.number = number
        self.suit = suit

    def __eq__ (self, other):
        if type(other) is self.__class__:
            return (self.__dict__ == other.__dict__)
        else:
            return False
        
#
# 7-9 In memory file system
#

class IdGen:
    def __init__(self):
        self.nextId = 0

    def gensym(self):
        id = self.nextId
        self.nextId +=1
        return id

class Node:
    # A node is a name that could be either a file or a dir
    def __init__(self, name, id, isDir):
        self.name = name
        self.id = id
        self.isDir = isDir
        self.parentId = None
        
    
class NodeFactory:
    def __init__(self):
        self.idGen = IdGen()
        id = self.idGen.gensym()
        self.root = Node('', id, True)

    def mkDir(self, parent, name):
        assert(parent.isDir)
        id = self.idGen.gensym()
        dir = Node(name, id, True)
        dir.parentId = parent.id
        return dir

    def newFile(self, parent, name):
        assert(parent.isDir)
        id = self.idGen.gensym()
        f = Node(name, id, False)
        f.parentId = parent.id
        return f

def list_dir(nodes, node):
    x = [y for y in nodes if \
                 (y.parentId == current_node.id)]
    return x				 
		
def find_node(nodes, cwd, root):
    if cwd[-1] == '/': # Trailing backslash is ignored
	    cwd = cwd[:-1]
    names = cwd.split('/')
    current_node = root
    for name in names:
        if len(name) == 0:
            current_node = root
        else:
            x = [y for y in nodes if \
                 (y.parentId == current_node.id) and \
                 (y.name == name)]
            if len(x) <1: 
                raise ValueError("Cannot find %s" % name)
            else:
                current_node = x[0] 
    return current_node

