# O(n) complexity since set is implemented as hash table
def contains_sum(numbers, x):
    s = set(numbers)
    for y in numbers:
        z = x -y
        # Remove the second condition
        # if the two numbers can be the same 
        if (z in s) and (z != y):  
            return True
    return False

if __name__ == "__main__":
    if contains_sum([1,3,4,5], 7):
        print("Pass")
    if not contains_sum([1,3,4,5], 2):
        print("Pass")
