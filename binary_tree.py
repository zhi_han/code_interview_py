class Node:
    def __init__(self, x):
        self.data = x
        self.left = None
        self.right = None
    def __str__(self):
        return "Node(" + str(self.data) + ")"

    def disp_tree(self):
        print(self.data, end="")
        print("(", end = "")
        if not (self.left is None):
            self.left.disp_tree()
        print(",", end ="" )
        if not (self.right is None):
            self.right.disp_tree()
        print(")", end ="")

    def height(self):
        leafH = []
        def visitNode(node, h):
            if (node.left is None) and (node.right is None):
                leafH.append(h)
            else:
                if not(node.left is None):
                    visitNode(node.left, h+1)
                if not(node.right is None):
                    visitNode(node.right, h+1)
        visitNode(self, 1)
        return max(leafH)


# Given an array, create a binary tree with minimal height
# The idea is to use a bread-first visit to create nodes
def create_tree(l):
    work = l # working list
    if len(work) > 0:
        e = work.pop()
        node = Node(e)        
        #BFS
        
        queue = [node]
        while (len(work) > 0):
            current_node = queue[0]
            queue = queue[1:]

            e = work.pop()
            l = Node(e)
            current_node.left = l
            queue.append(l)

            if len(work) >0:
                # continue to expand right
                e = work.pop()
                r = Node(e)
                current_node.right = r
                queue.append(r)
    else:
        return None
    return node

# Given a binary search tree, create linked list for each
# level.

# Instead of using linked list, we will be using python list.

def create_lists(t):
    result = [] 
    working = []
    
    queue = [(t,1)]
    current_level = 1
    while (len(queue) > 0):
        (node, level) = queue[0]
        queue = queue[1:]
        
        if not (node.left is None):
            queue.append((node.left, level+1))
        if not (node.right is None):
            queue.append((node.right, level+1))

        if level > current_level:
            result.append(working)
            working = [node.data]
            current_level = level
        else:
            working.append(node.data)
    # Last level
    result.append(working)
    return result

#
# With back pointer
class NodeWithParent(Node):
    def __init__(self, x):
        super().__init__(x)
        self.parent = None

    def update_parent(self):
        if not (self.left is None):
            self.left.parent = self
            self.left.update_parent()
        if not (self.right is None):
            self.right.parent = self
            self.right.update_parent()
    
    def get_next(self):
        def down_left(node):
            while (node.left is not None):
                node = node.left
            return node
        def backtrack_until_left(node):
            while (True):
                if (node.parent is not None):
                    if (node is node.parent.right):
                        node = node.parent
                    else:
                        node = node.parent
                        break
                else:     
                    return None
            return node
            
        if (self.right is not None) :
            return down_left(self.right)
        else:
            return backtrack_until_left(self)
        

#
# Sum nodes
def print_sums(root, n):
    def print_list(l):
        s = [str(i) for i in l]
        print(",".join(s))

    def sum_to(l):
        m = len(l)
        total = 0
        for i in range(m):
            subl = l[i:]
            if sum(subl) == n:
                total +=1
                print_list(subl)
        return total
    # 
    def visit_node(node, l):
        total = sum_to(l)
        
        if (node.left is not None):
            total += visit_node(node.left, l + [node.left.data])
        if (node.right is not None):
            total += visit_node(node.right, l + [node.right.data])
        return total

    return visit_node(root, [root.data])

def match_it(t, st):
    if st is None:
        return True
    elif t is None:
        return False
    else:
        if (t.data != st.data):
            return False
        matched = match_it(t.left, st.left)
        if (not matched):
            return False
        else:
            return match_it(t.right, st.right)
        


class RedBlackTree(Node):
    def __init__(self, x):
        super().__init__(x)
        self.parent = None
        self.color = 'b'

    def rotate_right(self):
        # Left child become parent, 
        # parent become right child
        parent = self
        leftChild = self.left

        newRightChild = Node(parent.data)
        newRightChild.left = leftChild.right
        newRightChild.right = parent.right
        newRightChild.color = parent.color
        
        parent.data = leftChild.data
        parent.color = leftChild.color
        parent.left = leftChild.left
        parent.right = newRightChild

    def rotate_left(self):
        parent = self
        rightChild = self.right

        newLeftChild = Node(parent.data)
        newLeftChild.right = rightChild.left
        newLeftChild.left = parent.left
        newLeftChild.color = parent.color

        parent.data = rightChild.data
        parent.color = rightChild.color
        parent.left = newLeftChild
        parent.right = rightChild.right

        
    def addLeft(self, y):
        y.parent = self
        self.left = y

    def addRight(self, x):
        self.right = x
        x.parent = self
        
    def insert(self, z):
        x = self
        y = None
        while x is not None:
            y = x
            if z < x.data:
                x = x.left
            else:
                x = x.right
        newNode = RedBlackTree(z)
        if z < y.data:
            y.addLeft(newNode)
        else:
            y.addRight(newNode)
        if (newNode.parent.parent is not None):
            self.fixup(newNode)

    def fixup(self, z):
        # Find z's uncle
        z.color = 'r'
        p = z.parent
        while (p.color == 'r'):
            if (p is p.parent.left):
                uncle = p.parent.right
            else:
                uncle = p.parent.left
            if uncle.color == 'r': # Case 1: change color
                p.color = 'b'
                uncle.color = 'b'
                p.parent.color = 'r'
                # Loop variables
                z = z.parent.parent
                p = z.parent
                # print("Case 1")
                # self.disp_tree()
                # print("")
            else:  # uncle color is b, case 2: move and rotate
                if (z is p.right):
                    z = p
                    z.rotate_left() 
                    p = z
                    z = p.left
                    p.color = 'b' # Case 3
                    p.parent.color = 'r'
                    p.parent.rotate_right()
                    # print("Case 2")
                    # self.disp_tree()
                    
                else:
                    z = p
                    z.rotate_right()
                    z.color = 'b'
                    p = z
                    z = p.right
                    p.color = 'b'
                    p.parent.color = 'r'
                    p.parent.rotate_left()
                    # print("Case 3:", end= "")
                    # self.disp_tree()

        self.color = 'b'
