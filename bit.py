# Bit manipulation

def copy_bits(n, m, i, j):
    mask = (1 << i) - (1 << j)
    n = n & (~mask)
    n = n | (mask & m << j)
    return n

def msb(x):
    for i in range(31, -1, -1):
        t = 1 << i
        if (x & t) != 0:
            return i
    return None
    
def bit_at(n, i):
    x = n & (1 << i)
    return (x >> i)
def set_bit(n, i):
    return n | (1 << i)
def clear_bit(n, i):
    return n & (~(1<<i))
def next_small(n):
    
    i = 1
    while (bit_at(n,i) == 1):
        i = i+1
    
    m = n
    m = set_bit(m, i)
    m = clear_bit(m, i-1)
    return m
