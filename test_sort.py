import unittest
import random

from sort import *

class SortTest(unittest.TestCase):
    def test1(self):
        a = list(range(32))
        random.shuffle(a)
        x = a[0:10]
        y = a[10:]
        x.sort()
        y.sort()

        merged = merge_sorted(x,y)
        self.assertEqual(merged, sorted(a))

    def testPyramid(self):
        l = []
        l.append(Man(65, 100))
        l.append(Man(70, 150))
        l.append(Man(56,90))
        l.append(Man(75, 190))
        l.append(Man(60, 95))
        l.append(Man(68, 110))
        r = longest_pyramid(l)
        
        #print(",".join(s))
        self.assertEqual(len(r), 6)

if __name__ == "__main__":
    unittest.main()
