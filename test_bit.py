import unittest
from bit import *

class BitTest(unittest.TestCase):
    def test_copy(self):
        n = int('101', 2)
        m = int('1', 2)
        x = copy_bits(n, m, 2, 1)
        result = int('111', 2)
        self.assertEqual(x, result)
    def test_next(self):
        self.assertEqual(next_small(5),6)
        self.assertEqual(next_small(7), 11)
    

if __name__ == "__main__":
    unittest.main()
