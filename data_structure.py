class Stack:
    #
    def __init__(self):
        self.data = []
        return

    def push(self, x):
        self.data.append(x)
        return

    def isempty(self):
        return (not self.data)

    def pop(self):
        if self.isempty():
            raise ValueError("Stack is empty")
        x = self.data[-1]
        self.data = self.data[:-1]
        return x


