# Test for find_triples.py

import unittest
from find_triples import *

class FindTripleTest(unittest.TestCase):
    def testFindLower(self):
        """Test for the utility function that find lower bound"""
        s = [1, 2, 3, 4, 5]
        self.assertEqual(BinarySearch.find_lower(s, 6), 4)
        self.assertEqual(BinarySearch.find_lower(s, 0), -1)
        self.assertEqual(BinarySearch.find_lower(s, 3), 2)
        self.assertEqual(BinarySearch.find_lower(s, 2), 1)
        self.assertEqual(BinarySearch.find_lower(s, 5), 4)

        s = [1, 2, 2, 2, 4]
        self.assertEqual(BinarySearch.find_lower(s, 2), 3)

    def testFindUpper(self):
        """Test for the utility function that find upper bound"""
        s = [1, 2, 3, 4, 5]
        self.assertEqual(BinarySearch.find_upper(s, 6), 5)
        self.assertEqual(BinarySearch.find_upper(s, 0), 0)
        self.assertEqual(BinarySearch.find_upper(s, 3), 2)
        self.assertEqual(BinarySearch.find_upper(s, 2), 1)
        self.assertEqual(BinarySearch.find_upper(s, 5), 4)

        s = [1, 2, 2, 2, 4]
        self.assertEqual(BinarySearch.find_upper(s, 2), 1)

    def testCountLower(self):
        s = [1, 2, 3, 4]
        self.assertEqual(count_lower(s, 3), 3)

    def testCountUpper(self):
        s = [1, 2, 3, 4]
        self.assertEqual(count_upper(s, 3), 2)

    def testCountTriples(self):
        s = [1, 2, 3, 4, 5]
        # (1,1,1) (1,2,1) (1,1,2) (2,1,1)
        self.assertEqual(count_triples(s, 4), 4) 
        self.assertEqual(count_triples(s, 1), 0)
        self.assertEqual(count_triples(s, 3), 1)
        self.assertEqual(count_triples(s, 100), 125)

if __name__ == "__main__":
    unittest.main()
