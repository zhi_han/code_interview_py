# Given a set of n integers and a number x, find the triples that satisfies 
#  a + b + c <= x
# 


class BinarySearch:

    @staticmethod
    def find_lower(sorted_list, x):
        """
        Finds the lower bound of a sorted list using binary search. That is,
        the largest index i that sorted_list[i] <= x.
        """
        def find_rec(l, u):
            if (l < u):
                mid = int((l + u) / 2)
                if (mid == l):
                    return l
                elif (sorted_list[mid] <= x):
                    return find_rec(mid, u)
                else:
                    return find_rec(l, mid)
            else:
                return l
        if (x < sorted_list[0]):
            return  -1
        elif (x >= sorted_list[-1]):
            return len(sorted_list) - 1
        else:
            return find_rec(0, len(sorted_list) - 1)

    @staticmethod
    def find_upper(sorted_list, x):
        """
        Finds the upper bound of a sorted list using binary search. That is,
        the smallest index i that sorted_list[i] >= x.
        """
        def find_rec(l, u):
            if (l < u):
                mid = int((l + u) / 2)
                if (mid == l):
                    return u
                elif (sorted_list[mid] < x):
                    return find_rec(mid, u)
                else:
                    return find_rec(l, mid)
            else:
                return l
        if (x < sorted_list[0]):
            return  0
        elif (x > sorted_list[-1]):
            return len(sorted_list)
        else:
            return find_rec(0, len(sorted_list) - 1)
            

def count_lower(sorted_list, x):
    """ Counts the number of elements i s.t. i <= x """
    return BinarySearch.find_lower(sorted_list, x) + 1

def count_upper(sorted_list, x):
    """ Counts the number of elements i s.t. i >= x """
    return len(sorted_list) - BinarySearch.find_upper(sorted_list, x)

def count_triples_more(nums, x):
    """Counts the number of triples."""
    # Use O(n^2) space and O(n^2 log(n)) time
    sums_of_two = []
    for a in nums:
        sums_of_two.extend([a + b for b in nums])
    sums_of_two.sort()

    total = 0
    for c in nums:
        total += count_lower(sums_of_two, x - c)
    return total

def count_triples(nums, x):
    """Counts the number of triples."""
    # Use O(n) space and O(n^2 log(n)) time
    x_minus_c = [x - c for c in nums]
    x_minus_c.sort()

    total = 0
    for a in nums:
        for b in nums:
            total += count_upper(x_minus_c, a + b)
    return total