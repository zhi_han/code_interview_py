import unittest
import math
import random

from heap import *

class HeapTest(unittest.TestCase):
    def testDown(self):
        nodes = [HeapNode(i) for i in range(7,0,-1)]
        for i in range(math.floor(7/2)):
            nodes[i].insert_left(nodes[2*i+1])
            nodes[i].insert_right(nodes[2*i+2])
        self.assertTrue(nodes[0].verify())

        nodes[0].data = -1
        nodes[0].percolate_down()

        self.assertTrue(nodes[0].verify())

    def testUp(self):
        nodes = [HeapNode(i) for i in range(7,0,-1)]
        for i in range(math.floor(7/2)):
            nodes[i].insert_left(nodes[2*i+1])
            nodes[i].insert_right(nodes[2*i+2])
 
        nodes[6].data = 8
        nodes[6].percolate_up()
        self.assertTrue(nodes[0].verify())

    def testHeapify(self):
        a = list(range(7))
        random.shuffle(a)
        nodes = [HeapNode(i) for i in a]
        for i in range(math.floor(7/2)):
            nodes[i].insert_left(nodes[2*i+1])
            nodes[i].insert_right(nodes[2*i+2])
        for n in nodes:
            n.percolate_up()
        self.assertTrue(nodes[0].verify())

    def testHeapSort(self):
        a = list(range(48))
        b = heap_sort(a)
        a.sort()
        a.reverse()
        self.assertEqual(b, a)
    
    def testHeapMedian(self):
        a = list(range(96))
        random.shuffle(a)
        self.assertEqual(median_heaps(a).data, -48)
        
if __name__ == "__main__":
    unittest.main()
