import unittest
from amazon import *

class AmazonTest(unittest.TestCase):
	def test_odd_nums(self):
		x = [1,2, 2, 2, 2, 3, 4, 4]
		y = odd_nums(x)
		self.assertTrue( 1 in y)
		self.assertTrue( 3 in y)

if __name__ == "__main__":
	unittest.main()
