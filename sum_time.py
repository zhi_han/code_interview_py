import sys

# Example: python3.3 sum_time.py "00:02:50" "01:02:40"
def calc_time(s):
    ss = s.strip()
    parts = ss.split(":")
    hour = int(parts[0])
    minute = int(parts[1])
    sec = int(parts[2])
    t = sec + (minute + hour * 60) * 60
    return t

def print_time(t):
    sec = t % 60
    t2 = t / 60
    minute = t2 % 60
    hour = t2 / 60
    print("%d:%d:%d" %(hour, minute, sec))


if (__name__ == "__main__"):
    t1 = calc_time(sys.argv[1])
    t2 = calc_time(sys.argv[2])
    t3 = (t1 + t2) / 2
    print_time(t3)
