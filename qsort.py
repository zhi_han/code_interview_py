""" Quick sort algorithm implemented in python. """

def partition(l):
    # Use the head element to partition the list
    n = len(l)
    empty_space = 0
    x = l[0]

    j = 1
    while j < n:
        if l[j] < x:
            l[empty_space] = l[j]
            l[j] = l[empty_space+1]
            empty_space += 1
        j += 1
    l[empty_space] = x
    return empty_space

def quick_sort(l):
    work = l
    i = partition(work)
    first = l[0:i]
    second = l[i+1:]
    result = first
    if len(first) > 1:
        result = quick_sort(first)
    result.append(l[i])
    if len(second) > 1:
        r = quick_sort(second)
        result.extend(r)
    else:
        result.extend(second)
    return result
