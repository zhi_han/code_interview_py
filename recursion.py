def permute_string(s):
    if len(s) == 1:
        return [s]
    elif len(s) == 0:
        return []
    else:
        n = len(s)
        head = s[0]
        permute_rest = permute_string(s[1:])
        result = []
        for i in range(n):
            for rest in permute_rest:
                e = rest[0:i] + head + rest[i:]
                result.append(e)
        return result


# cents
def num_cents(n, coins):
    if len(coins) == 1:
        # Only 1 cents
        assert(coins[0] == 1)
        return 1
    else:
        pos = int(n/coins[0])
        other_coins = coins[1:]
        x = pos
        total = 0
        while (x >=0):
            remain = n - x*coins[0]
            total += num_cents(remain, other_coins)
            x -=1 
        return total

# Paren
def paren(n):
    if n == 1:
        return ['()']
    else:
        x = paren(n-1)
        r = ['()' + i for i in x]
        n = ['(' + i + ')' for i in x]
        
        return r + n
    
