import unittest
from b_tree import *

class BTreeUnit(unittest.TestCase):
    def test_split(self):
        print("Test_slit")
        root = BTree(4)
        l = BTree(0)
        l.keys.append(2)
        r = BTree(5)
        root.addChild(0, l)
        root.addChild(1, r)
        
        root.disp_tree()
        print("")
        root.insert(1)
        root.disp_tree()
        print("")
        self.assertEqual(len(root.children), 3)

    def test_split2(self):
        root = BTree(4)
        l = BTree(0)
        l.keys.append(2)
        r = BTree(5)
        r.keys.append(7)
        root.addChild(0, l)
        root.addChild(1, r)
        root.disp_tree()
        print("")
        root.insert(6)
        root.disp_tree()
        print("")
        self.assertEqual(len(root.children), 3)

    def test_knuth(self):
        root = BTree(1)
        for i in range(2,8):
            root.insert(i)
            root.disp_tree()
            print("")
        self.assertEqual(len(root.children), 2)
        self.assertEqual(root.keys, [4])

if __name__ == "__main__":
    unittest.main()
