# 2-3 B-tree

class BTree:
    def __init__(self, key):
        self.keys = [key]
        self.children = []
        self.parent = None

    def disp_tree(self):
        keystr = [str(k) for k in self.keys]
        print("[" + ",".join(keystr) + "]", end= "")
        print("(", end = "")
        for c in self.children:
            if c is not None:
                c.disp_tree()
                if c is not self.children[-1]:
                    print(",", end = "")
        print(")", end ="")

    def addChild(self, i, y):
        y.parent = self
        if (i < len(self.children)):
            self.children.insert(i, y)
        else:
            self.children.append(y)

    def removeChild(self, y):
        y.parent = None
        self.children.remove(y)

    def split_leaf(self, new_key):
        # Return a subtree of splitting
        keys = self.keys
        keys.append(new_key)
        assert(len(keys) == 3)
        keys.sort()
        
        leftNode = BTree(keys[0])
        self.addChild(0, leftNode)
        # Create new child
        rightNode = BTree(keys[2])
        self.addChild(1, rightNode)
        self.keys = [keys[1]]


    def split_interior(self, i):
        new_subtree = self.children[i]
        assert(len(new_subtree.children) == 2)
        assert(len(new_subtree.keys) == 1)
        assert(len(self.children) == 3)
        assert(len(self.keys) == 2)
        if (i == 0):
            # Left 
            newNode = BTree(self.keys[1])
            l = self.children[1]
            r = self.children[2]
            self.removeChild(l)
            self.removeChild(r)
            self.addChild(1, newNode)
            newNode.addChild(0, l)
            newNode.addChild(1, r)
            self.keys = [self.keys[0]]
        elif (i == 2):
            # Create new node for left branch
            newNode = BTree(self.keys[0])
            l = self.children[0]
            r = self.children[1]
            self.removeChild(l)
            self.removeChild(r)
            self.addChild(0, newNode)
            newNode.addChild(0, l)
            newNode.addChild(1, r)
            self.keys = [self.keys[1]]
        else:
            # Create new node for left branch
            newNode = BTree(self.keys[0])
            l = self.children[0]
            r = new_subtree.children[0]
            self.removeChild(l)
            new_subtree.removeChild(r)
            newNode.addChild(0, l)
            newNode.addChild(1, r)
            
            newNode2 = BTree(self.keys[1])
            l = new_subtree.children[1]
            r = self.children[2]
            new_subtree.removeChild(l)
            self.removChild(r)
            newNode2.addChild(0, l)
            newNode2.addChild(0, r)

            self.removeChild(new_subtree)

            self.keys = newsubtree.keys
            self.addChild(0, newNode)
            self.addChild(1, newNode2)

    def absorb(self, i):
        # Copy and paste the i-th child after splitting
        # the i-the child
        child = self.children[i]
        if len(self.children) < 3:
            assert(len(child.keys) == 1)
            self.keys.insert(i, child.keys[0])
            # Move the children
            self.removeChild(child)
            x = child.children[1]
            y = child.children[0]
            child.removeChild(x)
            child.removeChild(y)
            self.addChild(i, x)
            self.addChild(i, y)
        else:
            self.split_interior(i)
            if self.parent is not None:
                myIdx = self.parent.children.index(self)
                self.parent.absorb(myIdx)

    def insert(self, new_key):
        # Look for the right node to insert
        if len(self.children) > 0:
            n = len(self.keys)
            for i in range(len(self.keys)):
                if new_key < self.keys[i]:
                   self.children[i].insert(new_key)
            if new_key > self.keys[-1]:
                self.children[-1].insert(new_key)
        else:
            if len(self.keys) < 2:
                self.keys.append(new_key)
                self.keys.sort()
            else:
                # Full
                print("Split for %d" % new_key)
                self.split_leaf(new_key)
                if self.parent is not None:
                    myIdx = self.parent.children.index(self)
                    self.parent.absorb(myIdx)
        
            
        
