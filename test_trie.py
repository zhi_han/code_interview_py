import unittest
from trie import *

class TrieTest(unittest.TestCase):
    def test_simple(self):
        a = Trie("")
        n1 = a.insert("011")
        a.insert("100")
        n2 = a.insert("1011")
        s = a.fullname(n2)
        self.assertEqual(s, "1.0.1.1")
        self.assertEqual(a.fullname(n1), "0.1.1")

if __name__ == "__main__":
    unittest.main()
