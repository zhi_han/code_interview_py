import unittest
from anagram import is_anagram

class AnagramTest(unittest.TestCase):
    def testAnagram1(self):
        self.assertTrue(is_anagram("abcd", "bcad"))
        return

    def testAnagram2(self):
        self.assertFalse(is_anagram("abcd", "bcaa"))
        return

if __name__ == "__main__":
    unittest.main()
                           
