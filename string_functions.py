""" String functions 
This modules has a few C-style string functions
"""

""" Pattern matching """
def match_char_list(pat, l):
    if (len(pat) == 0):
        return (len(l) == 0)
    elif (len(l) == 0):
        if (len(pat) == 0):
            return True
        elif pat[0] == "*":
            return match_char_list(pat[1:], l)
    elif pat[0] == "?":
        return match_char_list(pat[1:], l[1:])
    elif pat[0] == "*":
        return match_char_list(pat[1:], l) or \
            match_char_list(pat, l[1:])
    else:
        if pat[0] == l[0]:
            return match_char_list(pat[1:], l[1:])
        else:
            return False

def pat_match(pat, s):
    return match_char_list(list(pat), list(s))

""" Swap in place"""
def reverse_char_list(l):
    n = len(l)
    head = 0
    tail = n-1
    while head < tail:
        (l[head], l[tail]) = (l[tail], l[head])
        head += 1
        tail -= 1
    
def reverse_string(s):
    l = list(s)
    reverse_char_list(l)
    return("".join(l))
