import unittest
from data_structure import *

class DataStructureTest(unittest.TestCase):
    def testStack(self):
        s = Stack()
        self.assertTrue(s.isempty())
        s.push(1)
        s.push(2)
        self.assertFalse(s.isempty())
        
        x = s.pop()
        self.assertEqual(x, 2)
        x = s.pop()
        self.assertEqual(x, 1)
        
        self.assertTrue(s.isempty())
        


if __name__ =="__main__":
    unittest.main()
