""" Google interview problems """

"""
You have k lists of sorted integers. Find the smallest range that
includes at least one number from each of the k lists.

For example,
List 1: [4, 10, 15, 24, 26]
List 2: [0, 9, 12, 20]
List 3: [5, 18, 22, 30]

The smallest range here would be [20, 24] as it contains 24 from list
1, 20 from list 2, and 22 from list 3.

"""

def min_range(l):
    n = len(l)
    idx = [0 for i in range(n)]
    
    stop = False
    while not stop:
        k = [ (i, l[i][idx[i]]) for i in range(n)]
        k.sort(key = lambda x: x[1])
        (j, val) = k[0]
        if idx[j] == len(l[j]) -1 :
            stop = True
        else:
            idx[j] = idx[j] + 1
    result = [l[i][idx[i]] for i in range(n)]
    return result
        

""" Give you an array which has n integers,it has both positive and
negative integers.Now you need sort this array in a special way.After
that,the negative integers should in the front,and the positive
integers should in the back.Also the relative position should not be
changed.  eg. -1 1 3 -2 2 ans: -1 -2 1 3 2.  """

def stable_partition(l):
    pos = [x for x in l if x >0]
    neg = [y for y in l if y < 0]
    return (pos, neg)

""" Given an array of integers. Find two disjoint contiguous sub-
arrays such that the absolute difference between the sum of two sub-
array is maximum. * The sub-arrays should not overlap.

eg- [2 -1 -2 1 -4 2 8] ans - (-1 -2 1 -4) (2 8), diff = 16 
"""
def max_diff(l):
    def find_max_or_min(l, find_max):
        m = 0
        ex = l[0]
        for i in range(1,len(l)):
            if find_max:
                if l[i] > ex:
                    m = i
                    ex = l[i]
            else:
                if l[i] < ex:
                    m = i
                    ex = l[i]
        return m 
    #   Cumulative sum
    def csum(l):
        cumsum = []
        s = 0
        for i in l:
            s += i
            cumsum.append(s)
        return cumsum
    def find_two_extremes(cumsum):
        max_i = find_max_or_min(cumsum, True)
        min_i = find_max_or_min(cumsum, False)
        return sorted((min_i, max_i))

    # Main routine goes here
    cs = csum(l)
    (i, j) = find_two_extremes(cs)
    if j == len(l) -1:
        (i2, j2) = find_two_extremes(cs[:i+1])
        return (i2, i)
    elif i == 0:
        (i2, j2) = find_two_extremes(cs[j:])
        return (j, j + j2)
    else:
        return (i, j)
