import math
""" Heap data structure implement in python """

""" A Max heap """
class HeapNode:
    def __init__ (self,  data):
        self.data = data
        self.left = None
        self.right = None
        self.parent = None

    def insert_left(self, c):
        self.left = c
        c.parent = self
    def insert_right(self, c):
        self.right = c
        c.parent = self        

    def disp_tree(self):
        print(self.data, end="")
        print("(", end = "")
        if not (self.left is None):
            self.left.disp_tree()
        print(",", end ="" )
        if not (self.right is None):
            self.right.disp_tree()
        print(")", end ="")

    def percolate_down(self):
        # Compute the large child between the left and right
        largeChild = None
        if self.left is not None:
            if self.data < self.left.data:
                largeChild = self.left
        if self.right is not None:
            if self.data < self.right.data:
                if largeChild is not None:
                    if largeChild.data < self.right.data:
                        largeChild = self.right
                else:
                    largeChild = self.right

        # Percolate down if a large child is found
        if largeChild is not None:
            (self.data, largeChild.data) = (largeChild.data, self.data)
            largeChild.percolate_down()

    def percolate_up(self):
        if self.parent is not None:
            if self.data > self.parent.data:
                (self.data, self.parent.data) = (self.parent.data, self.data)
                self.parent.percolate_up()
        

    def verify(self):
        valid = True
        if self.left is not None:
            valid = (self.data > self.left.data) and \
                self.left.verify()
        if self.right is not None:
            valid = valid and (self.data > self.right.data) and \
                self.right.verify()
        return valid

    def detach(self):
        p = self.parent
        if p is not None:
            if p.left is self:
                p.left = None
            else:
                p.right = None
            self.parent = None

    

def heap_sort(l):
    nodes = [HeapNode(i) for i in l]
    for i in range(math.ceil(len(l)/2)-1):
        nodes[i].insert_left(nodes[2*i+1])
        nodes[i].insert_right(nodes[2*i+2])
    if len(l) % 2 == 0:
        nodes[int(len(l)/2)-1].insert_left(nodes[-1])
    # Heapify
    for n in nodes:
        n.percolate_up()

    i = len(nodes)

    result = []
    while i >0:
        largest = nodes[0].data
        result.append(largest)

        tail = nodes[i-1].data
        nodes[i-1].detach()        

        nodes[0].data = tail
        nodes[0].percolate_down()
        i -= 1

    return result

def create_heap(l):
    nodes = [HeapNode(i) for i in l]
    for i in range(math.ceil(len(l)/2)-1):
        nodes[i].insert_left(nodes[2*i+1])
        nodes[i].insert_right(nodes[2*i+2])
    if len(l) % 2 == 0:
        nodes[int(len(l)/2)-1].insert_left(nodes[-1])
    return nodes

def median_heaps(l):
    n = math.floor(len(l)/2)
    lower = l[0:n]
    upper = [-x for x in l[n:]] # negative values for min-heap
    
    
    # Heapify the two heaps 
    lowerHeap = create_heap(lower)
    for n in lowerHeap:
        n.percolate_up()
    upperHeap = create_heap(upper)
    for n in upperHeap:
        n.percolate_up()

    while (-upperHeap[0].data) < lowerHeap[0].data:
        s = lowerHeap[0].data
        lowerHeap[0].data = - upperHeap[0].data
        lowerHeap[0].percolate_down()
        
        upperHeap[0].data = - s
        upperHeap[0].percolate_down()
    
    return upperHeap[0]
    

    
