#!/usr/local/bin/python

# Example: python3.3 count_letters.py "This is 1 string of test"
import sys

def add_dict(d, k):
    if k in d:
        d[k] = d[k] + 1
    else:
        d[k] = 1

def count_letters(s):
    digits = dict()
    letters = dict()
    for c in s:
        if c.isalnum():
            if c.isdigit():
                add_dict(digits, c)
            else:
                add_dict(letters, c)
    return(digits, letters)

def print_result(d):
    keys = sorted(d.keys())
    for k in keys:
        print(k, end=" ")
        print(d[k])


if (__name__ == "__main__"):
    s = " ".join(sys.argv[1:])
    (digits, letters) = count_letters(s.lower())
    print_result(digits)
    print_result(letters)
