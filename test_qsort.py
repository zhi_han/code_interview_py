import unittest
import random
from qsort import *

class TestQuickSort(unittest.TestCase):
    def test1(self):
        x = list(range(80))
        random.shuffle(x)
        self.assertEqual(quick_sort(x), sorted(x) )

if __name__ == "__main__":
    unittest.main()
