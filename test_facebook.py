import unittest
import random
from facebook import *

class FacebookTest(unittest.TestCase):
    def testPathConcat(self):
        p = PathConcat()
        x = p.concat_paths('/usr/bin/mail', \
                           '../../xyz/../abc')
        self.assertEqual(x, '/usr/abc')

    def test_palindrome(self):
        self.assertTrue(k_palindrome(list("abxa"), 1))
        self.assertTrue(k_palindrome(list("axba"), 1))
        self.assertFalse(k_palindrome(list("abdxa"), 1))

    def test_decode_num(self):
        s = "1123"
        l = decode_num(s)
        # for c in l:
        #    print(c)
        self.assertEqual(len(l), 5)
    
    def test_is_match(self):
        self.assertTrue(is_match("a*", ""))
        self.assertFalse(is_match(".", ""))
        self.assertTrue(is_match("ab*", "a"))
        self.assertTrue(is_match("a.", "ab"))
        self.assertTrue(is_match("a", "a"))

    def test_move_elems(self):
        self.assertEqual(move_elems([2,3,1,0]), [1,0,3,2])
        self.assertEqual(move_elems([0,1,2,3]), [0,1,2,3])
        self.assertEqual(move_elems([1,0,3,2]), [0,1,2,3])

    def test_copy_list(self):
        numbers = list(range(6))
        order = list(range(6))
        random.shuffle(order)
        nodes = [Node(None, None, n) for n in numbers]
        for (i, n) in enumerate(nodes):
            if i != len(nodes) - 1 :
                n.next = nodes[i+1]
            n.random = nodes[order[i]]
        l = nodes[0].to_list()
        
        node2 = copy_list(nodes[0])
        l1 = nodes[0].to_list()
        l2 = node2.to_list()
        self.assertEqual(l, l1)
        self.assertEqual(l, l2)

if __name__ == "__main__":
    unittest.main()
