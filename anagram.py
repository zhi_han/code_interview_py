# O(n*log(n)) complexity due to the use of sort
def is_anagram(a, b):
    al = list(a)
    al.sort()
    bl = list(b)
    bl.sort()
    return (al == bl)


