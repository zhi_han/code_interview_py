""" Sort algorithms """

def merge_sorted(a,b):
    result = []
    i = 0
    j = 0
    n = len(a)
    m = len(b)

    while (i < n) and (j < m):
        if a[i] < b[j]:
            result.append(a[i])
            i += 1
        else:
            result.append(b[j])
            j += 1

    if (i < n):
        result.extend(a[i:])
    if (j < m):
        result.extend(b[j:])
    return result

""" Longest pyramid """
class Man:
    def __init__(self, h, w):
        self.h = h
        self.w = w
        self.next1 = None
        self.next2 = None
    def to_str(self):
        return "(" + str(self.h) + "," +\
            str(self.w) + ")"

def longest_pyramid(l):
    l.sort( key = lambda x: x.h)
    for i in range(len(l)):
        x = l[i]
        for j in l[i+1:]:
            if x.w < j.w:
                x.next1 = j
                break
    l.sort( key = lambda x: x.w)
    for i in range(len(l)):
        x = l[i]
        for j in l[i+1:]:
            if x.h < j.h:
                x.next2 = j
                break
    # Compute pyramid by dynamic programming
    p = dict()
    def update(n, np):
        if n in p:
            if len(p[n]) < len(np):
                p[n] = np + [n]
        else:
            p[n] = np + [n]
               

    for x in l:
        update(x, [])
        if x.next1 is not None:
            update(x.next1, p[x])
        if x.next2 is not None:
            update(x.next2, p[x])
    
    # Find the largest pyramid
    n = 0
    mp = p[l[n]]
    for x in l:
        if len(p[x]) > n:
            n = len(p[x])
            mp = p[x]
    return mp
    
