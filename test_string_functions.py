from string_functions import *
import unittest


class StringFunctionTest(unittest.TestCase):
    def test_match(self):
        self.assertTrue(pat_match("*", "abc"))
        self.assertTrue(pat_match("*", ""))
        self.assertTrue(pat_match("*a", "a"))
        self.assertTrue(pat_match("abc", "abc"))
        self.assertTrue(pat_match("a?c", "abc"))

        self.assertFalse(pat_match("", "abc"))
        self.assertTrue(pat_match("", ""))
        self.assertFalse(pat_match("*ab", "a"))
        self.assertFalse(pat_match("abc", "abcd"))
        self.assertFalse(pat_match("a?b", "abc"))

    def test_reverse_string(self):
        self.assertEqual(reverse_string("abc"), "cba")
        self.assertEqual(reverse_string("abcd"), "dcba")
        self.assertEqual(reverse_string("a"), "a")
        

if __name__ == "__main__":
    unittest.main()
