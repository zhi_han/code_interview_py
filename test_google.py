import unittest
from google import *

class GoogleTest(unittest.TestCase):
    def test_min_range(self):
        l = [ [4,10, 15,24, 26], \
              [0, 9, 12, 20], \
              [5, 18, 22, 30]]
        r = min_range(l)
        self.assertEqual(min(r), 20)
        self.assertEqual(max(r), 24)


    def test_stable_partition(self):
        (p, n ) = stable_partition([-1, 1, 3, -2, 2])
        self.assertEqual(p, [1,3,2])
        self.assertEqual(n, [-1, -2])

    def test_max_diff(self):
        l = [2, -1, -2, 1, -4, 2, 8]
        (i,j) = max_diff(l)
        self.assertEqual(i, 0)
        self.assertEqual(j, 4)

        # Second test case
        l = [2, 8, -1, -2, 1, -4, 2]
        (i,j) = max_diff(l)
        self.assertEqual(i, 1)
        self.assertEqual(j, 5)

        # Third test case
        l = [2, 1, -1, -2, 1, -4, 3]
        (i,j) = max_diff(l)
        self.assertEqual(i, 1)
        self.assertEqual(j, 5)

if __name__ == "__main__":
    unittest.main()
