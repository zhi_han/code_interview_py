import unittest
from recursion import *

class RecursionTest(unittest.TestCase):
    def testPermuteString(self):
        self.assertEqual(len(permute_string("abc")), 6)
    def testCoins(self):
        coins = [25, 10, 5, 1]
        self.assertEqual(num_cents(4, coins), 1)
        self.assertEqual(num_cents(6, coins), 2)
        self.assertEqual(num_cents(25, coins), 13)
    def testParen(self):
        self.assertEqual(len(paren(2)), 2)
        self.assertEqual(len(paren(3)), 4)
if __name__ == "__main__":
    unittest.main()
