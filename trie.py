class Trie:
    def __init__(self, c):
        self.data = c
        self.left = None
        self.right = None

    def disp_tree(self):
        print(self.data, end="")
        print("(", end = "")
        if not (self.left is None):
            self.left.disp_tree()
        print(",", end ="" )
        if not (self.right is None):
            self.right.disp_tree()
        print(")", end ="")

    def insert(self, fullname):
        l = list(fullname)
        cur = self
        for i in l:
            if i=="0":
                if cur.left is None:
                    cur.left = Trie("0")
                cur = cur.left
            else:
                if cur.right is None:
                    cur.right = Trie("1")
                cur = cur.right
        return cur

    def fullname(self, t):
        def find(node, t, s):
            if node is t:
                return ".".join(s)
            else:
                x = None
                if node.left is not None:
                    x = find(node.left, t, s + ["0"])
                if x is None:     
                    if node.right is not None:
                        x = find(node.right, t, s + ["1"])
                return x
        cur = self
        return find(cur, t, [])
        



            
