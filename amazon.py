""" Amazon questions """ """ You are given an integer array, where all
numbers except for TWO numbers appear even number of times.

Q: Find out the two numbers which appear odd number of times.
"""
def odd_nums(l):
    s = set()
    for n in l:
        if n in s:
            s.remove(n)
        else:
            s.add(n)
    return list(s)
