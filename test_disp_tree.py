import unittest
from binary_tree import *


class BinaryTreeTest(unittest.TestCase):
    def testTree(self):
        a = create_tree(list(range(2**7-1)))
        self.assertEqual(a.height(), 7)
    def testTree2(self):
        a = create_tree(list(range(2**7)))
        self.assertEqual(a.height(), 8)
    def testCreateList(self):
        a = create_tree(list(range(2**7)))
        l = create_lists(a)
        self.assertEqual(len(l), 8)
    def test_next(self):
        a = NodeWithParent("a")
        b = NodeWithParent("b")
        c = NodeWithParent("c")
        d = NodeWithParent("d")
        e = NodeWithParent("e")
        f = NodeWithParent("f")

        a.left = b
        a.right = c
        b.left = d
        b.right = e
        c.left = f
        a.update_parent()
        x = d
        n = 0
        while (x is not None):
            n += 1
            x = x.get_next()
        self.assertEqual(n, 6)

    def test_sum(self):
        a = create_tree(list(range(8-1)))
        # a.disp_tree()
        self.assertEqual(print_sums(a, 11), 2)
    def test_match(self):
        a = create_tree(list(range(8-1)))
        b = create_tree(list(range(7)))
        self.assertTrue(match_it(a,b))
        
        b.right = None
        self.assertTrue(match_it(a,b))
        
        a.left = None
        self.assertFalse(match_it(a,b))

    def test_rb_tree(self):
        a1 = RedBlackTree(1)
        a1.color = 'b'
        a2 = RedBlackTree(2)
        a2.color = 'r'
        a5 = RedBlackTree(5)
        a5.color = 'r'
        a7 = RedBlackTree(7)
        a7.color = 'b'
        a8 = RedBlackTree(8)
        a8.color = 'r'
        a11 = RedBlackTree(11)
        a11.color = 'b'
        a14 = RedBlackTree(14)
        a14.color = 'b'
        a15 = RedBlackTree(15)
        a15.color = 'r'
        
        root = a11
        a11.addLeft(a2)
        a11.addRight(a14)
        a2.addLeft(a1)
        a2.addRight(a7)
        a14.addRight(a15)
        a7.addLeft(a5)
        a7.addRight(a8)
        
        root.insert(4)
        
        root.disp_tree()
        print("")


if __name__ == "__main__":
    unittest.main()
