""" Facebook interview questions """
class PathConcat:
    def process_parts(self, p):
        x = [] # a stack 
        for name in p:
            if name == "..":
                if not x:
                    raise ValueError("at root level")
                else:
                    x = x[:-1]
            else:
                x.append(name)
        return x

    def concat_paths(self, a,b):
        asplit = a.split('/')
        bsplit = b.split('/')
        fullsplit = asplit + bsplit
        parts = self.process_parts(fullsplit)
        return "/".join(parts)

"""
A k-palindrome is a string which transforms into a palindrome
on removing at most k characters. 

Given a string S, and an interger K, print "YES" if S is a 
k-palindrome; otherwise print "NO". 

Constraints: 
S has at most 20,000 characters. 
0<=k<=30 

Sample Test Case#1: 
Input - abxa 1 
Output - YES 
Sample Test Case#2: 
Input - abdxa 1 
Output - No
"""

def k_palindrome(s, k):
    n = len(s)
    if n <= 1: 
        return True
    elif s[0] == s[n-1]:
        return k_palindrome(s[1:-1], k)
    elif k > 0:
        return k_palindrome(s[1:], k-1) or \
            k_palindrome(s[:-1], k-1)
    else:
        return False

        
""" 
If a=1, b=2, c=3,....z=26. Given a string, find all possible codes that string can generate. Give a count as well as print the strings. 

For example: 
Input: "1123". You need to general all valid alphabet codes from this string. 

Output List 
aabc //a = 1, a = 1, b = 2, c = 3 
kbc // since k is 11, b = 2, c= 3 
alc // a = 1, l = 12, c = 3 
aaw // a= 1, a =1, w= 23 
kw // k = 11, w = 23
"""

def decode_num(s):
    alphabet = [""]
    for i in range(0,26):
        alphabet.append(chr(ord('a') +i))

    def decode_rec(l):
        r = []
        if len(l) >= 1:
            v = alphabet[int(l[0])]
            remain = decode_rec(l[1:])
            r.extend([ v + x for x in remain])
        if len(l) >= 2:
            k = int(l[0] + l[1] )
            if k <= 26:
                v = alphabet[k]
                remain = decode_rec(l[2:])
                r.extend([v + x for x in remain])
        if len(l) == 0 :
            r.append("")
        return r
    
    return decode_rec(list(s))

"""
Given a regular expression with characters a-z, ' * ', ' . '  the task was
to find if that string could match another string with characters from: a-z
where

 ' * ' can delete the character before it, and 
' . ' could match whatever
character. ' * ' always appear after a a-z character.  

Example: isMatch("a*",
"") = true; isMatch(".", "") = false; isMatch("ab*", "a") = true; isMatch("a.",
"ab") = true; isMatch("a", "a") = true;
"""    

def is_match(x, y):
    xl = list(x)
    yl = list(y)

    # Normalize
    def normalize(l):
        result = []
        for e in l:
            if e == "*":
                result.pop()
            else:
                result.append(e)
        return result
    
    def match_reg(xl, yl):
        if len(xl) != len(yl):
            return False
        else:
            for (x,y) in zip(xl, yl):
                if x!="." and y != "." and x != y:
                    return False
        return True
    return match_reg(normalize(xl), normalize(yl))

"""
WAP to modify the array such that arr[I] = arr[arr[I]]. 
Do this in place i.e. with out using additional memory. 

example : if a = {2,3,1,0} 
o/p = a = {1,0,3,2} 
"""

def move_elems(ll):
    l = ll
    def move_all(i):
        last = l[i]
        n = l[i]
        j = i
        while n != i:
            l[j] = -1 - l[n]
            j = n
            n = l[n]
        l[j] = -1 -last
    for i in range(len(l)):
        if l[i] >= 0:
            move_all(i)
    for i in range(len(l)):
        l[i] = - l[i] -1
    return l

"""

Given a linked list where apart from the next pointer, every node also has a
pointer named random which can point to any other node in the linked list. Make
a copy of the linked list.

"""

class Node:
    def __init__(self, n, r, d):
        self.next = n
        self.random = r
        self.data = d

    def copy(self):
        return Node(self.next, self.random, self.data)

    def to_list(self):
        l = []
        n = self
        while n is not None:
            l.append((n.data, n.random.data))
            n = n.next
        return l

def copy_list(n):
    # First create an interleaving list
    node = n
    while node is not None:
        node_copy = node.copy()
        node.next = node_copy
        node = node_copy.next
    # Adjust random
    node = n
    while node is not None:
        copy_node = node.next
        copy_node.random = node.random.next
        node = node.next.next
    # Detacth
    node = n
    r = node.next
    while node is not None:
        copy_node = node.next
        node.next = copy_node.next
        if copy_node.next is not None:
            copy_node.next = copy_node.next.next
        node = node.next

    return r
